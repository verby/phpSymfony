<?php

namespace aw12\VitrineBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Validator\Constraints\DateTime;
use aw12\VitrineBundle\Entity\Panier;
use aw12\VitrineBundle\Entity\Commande;
use aw12\VitrineBundle\Entity\Client;
use aw12\VitrineBundle\Entity\LigneCommande;
use aw12\VitrineBundle\Services\MontantPanier;
use aw12\VitrineBundle\Services\Mail;
use aw12\VitrineBundle\Entity\Article;
use Symfony\Component\Security\Core\User\UserInterface;

class PanierController extends Controller {

    public function ajoutArticleAction($id, $qte = 1) {
        $session = $this->getRequest()->getSession();

        $panier = $session->get('panier', new Panier());
        $qte = $_POST['qte'];

        $repository = $this->getDoctrine()->getRepository('VitrineBundle:Article');
        $article = $repository->find($id);
        $panier->ajoutArticle($article, $qte);
        $session->set('panier', $panier);

        return $this->forward('VitrineBundle:Panier:contenuPanier');
    }

    public function contenuPanierAction() {
        $session = $this->getRequest()->getSession();
        $panier = $session->get('panier', new Panier());
        //Recherche d'articles
        $repository = $this->getDoctrine()->getRepository('VitrineBundle:Article');

        //Calculer le montant
        $articles = $panier->getContenu();
        $service = $this->container->get('montant_panier');
        $montant = $service->calculerMontant($panier);

        return $this->render('VitrineBundle:Panier:contenuPanier.html.twig', array('panier' => $articles, 'montant' => $montant,));
    }

    public function vuePanierAction() {
        $session = $this->getRequest()->getSession();
        $panier = $session->get('panier', new Panier());
        //Recherche d'articles
        $repository = $this->getDoctrine()->getRepository('VitrineBundle:Article');

        //Calculer le montant
        $montant = 0;
        $articles = $panier->getContenu();
        $service = $this->container->get('montant_panier');
        $montant = $service->calculerMontant($panier);
        return $this->render('VitrineBundle:Panier:vuePanier.html.twig', array('panier' => $articles));
    }

    public function viderPanierAction() {
        $session = $this->getRequest()->getSession();
        $session->remove('panier');
        return $this->forward('VitrineBundle:Panier:contenuPanier');
    }

    public function supprimerArticleAction($id) {
        $session = $this->getRequest()->getSession();

        $panier = $session->get('panier', new Panier());
        $panier->supprimeArticle($id);
        //Mis à jour du panier
        $session->set('panier', $panier);
        return $this->forward('VitrineBundle:Panier:contenuPanier');
    }

    public function validerPanierAction() {
        $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getManager();

        $panier = $session->get('panier', new Panier());
        $client = $this->get('security.context')->getToken()->getUser();

        //Créer une commande
        $commande = new Commande();
        $commande->setClient($client);
        $commande->setEtat(0);

        $dateCom = new \DateTime();
        $commande->setDateCom($dateCom);

        //Save Cammande
        $em->persist($commande);
        $em->flush();


        // Pour le mail
        $listeArticles = [];

        $repository = $this->getDoctrine()->getRepository('VitrineBundle:Article');
        foreach ($panier->getContenu() as $idArticle => $articles) {
            $ligne = new LigneCommande();
            $article = $repository->find($idArticle);
            $ligne->setArticle($article);
            $ligne->setCommande($commande);
            $ligne->setQuantite($articles['quantite']);
            $ligne->setPrix($articles['article']->getPrix() * intval($articles['quantite']));
            $em->persist($ligne);
            $em->flush();

            //Décendre la stock des articles
            $article->setStock($article->getStock() - $articles['quantite']);
            $em->persist($article);
            $em->flush();

            //echo count($articles);
            foreach ($articles as $unArticle) {
                // Bug : dernière ligne n'est pas un objet
                if (is_object($unArticle)) {
                    $listeArticles[] = $unArticle->getLibelle();
                }
            }
        }
        // Envoi d'un mail...
        $service = $this->container->get('mail');
        $service->sendMail("nadezhda@iureva.fr", $client->getMail(), "Commande", $listeArticles);

        //Vider le panier
        $panier->viderPanier();

        //Mis à jour du panier
        $session->set('panier', $panier);

        return $this->render('VitrineBundle:Panier:validationPanier.html.twig', array('commande' => $commande));
    }

}
