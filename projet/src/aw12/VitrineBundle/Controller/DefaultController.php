<?php

namespace aw12\VitrineBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use aw12\VitrineBundle\Entity\Panier;

class DefaultController extends Controller {

    public function indexAction() {
        return $this->render('VitrineBundle:Default:index.html.twig');
    }

    public function mentionsAction() {
        return $this->render('VitrineBundle:Default:mentions.html.twig');
    }

    public function catalogueAction() {
        $repository = $this->getDoctrine()->getRepository('VitrineBundle:Categorie');

        $categories = $repository->findAll();
        if (!$categories) {
            throw $this->createNotFoundException("Catégories null");
        }
        return $this->render('VitrineBundle:Default:catalogue.html.twig', array('categories' => $categories));
    }

    public function articlesParCategorieAction($id) {
        $repository = $this->getDoctrine()->getRepository('VitrineBundle:Categorie');

        $categorie = $repository->find($id);
        $articles = $categorie->getArticles();

        if (!$articles) {
            throw $this->createNotFoundException("Il n'y a pas d'articles pour cette catégorie");
        }
        return $this->render('VitrineBundle:Default:articlesParCategorie.html.twig', array('articles' => $articles));
    }

    public function plusVenduAction() {
        return $this->render('VitrineBundle:Default:plusVendu.html.twig');
    }

}
