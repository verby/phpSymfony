<?php

namespace aw12\VitrineBundle\Controller;

use aw12\VitrineBundle\Entity\Client;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Client controller.
 *
 */
class ClientController extends Controller {

    /**
     * Lists all client entities.
     *
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $clients = $em->getRepository('VitrineBundle:Client')->findAll();

        return $this->render('client/index.html.twig', array(
                    'clients' => $clients,
        ));
    }

    public function helloAction($name) {
        if (false === $this->get('security.context')->isGranted('ROLE_ADMIN')) {
            throw new AccessDeniedException();
        }
        $user = $this->getUser(); // C’est un objet de la classe Client !
    }

    public function newAction(Request $request) {
        $session = $this->getRequest()->getSession();

        $client = new Client();
        $form = $this->createForm('aw12\VitrineBundle\Form\ClientType', $client);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $em = $this->getDoctrine()->getManager();
            $encoder = $this->container->get('security.password_encoder');
            // On récupère l'encodeur défini dans security.yml
            $encoded = $encoder->encodePassword($client, $client->getPassword());
            // On encode le mot de passe issu du formulaire
            $client->setPassword($encoded);
            if ($client->getAdministrateur() == null) {
                $client->setAdministrateur(false);
            }

            $em->persist($client);
            $em->flush();

            $session->set('client', $client->getId());

            return $this->redirectToRoute('client_show', array('id' => $client->getId()));
        }

        $session->set('client', $client->getId());

        return $this->render('client/new.html.twig', array(
                    'client' => $client,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a client entity.
     *
     */
    public function showAction(Client $client) {
        $deleteForm = $this->createDeleteForm($client);

        return $this->render('client/show.html.twig', array(
                    'client' => $client,
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    public function showCommandesAction(Client $client) {
        $commandes = $client->getCommandes();

        return $this->render('commande/index.html.twig', array(
                    'commandes' => $commandes,
        ));
    }

    /**
     * Displays a form to edit an existing client entity.
     *
     */
    public function editAction(Request $request, Client $client) {

        $deleteForm = $this->createDeleteForm($client);
        $editForm = $this->createForm('aw12\VitrineBundle\Form\ClientType', $client);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted()) {
            $em = $this->getDoctrine()->getManager();
            $encoder = $this->container->get('security.password_encoder');
            // On récupère l'encodeur défini dans security.yml
            $encoded = $encoder->encodePassword($client, $client->getPassword());
            // On encode le mot de passe issu du formulaire
            $client->setPassword($encoded);
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('client_edit', array('id' => $client->getId()));
        }

        return $this->render('client/edit.html.twig', array(
                    'client' => $client,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a client entity.
     *
     */
    public function deleteAction(Request $request, Client $client) {
        $form = $this->createDeleteForm($client);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($client);
            $em->flush();
        }

        return $this->redirectToRoute('client_index');
    }

    /**
     * Creates a form to delete a client entity.
     *
     * @param Client $client The client entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Client $client) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('client_delete', array('id' => $client->getId())))
                        ->setMethod('DELETE')
                        ->getForm()
        ;
    }

}
