<?php

namespace aw12\VitrineBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;

class Panier {

    private $contenu;

    //Tableau - contenu[i] = quantite d'article d’id=i dans le panier)
    public function __construct() {
        // initialise le contenu
        $this->contenu = array();
    }

    public function getContenu() {
        return $this->contenu;
    }

    public function ajoutArticle($article, $qte = 1) {

        //Si idArticle n'est pas dans le tableau + ligne
        if (!array_key_exists($article->getId(), $this->contenu)) {
            // ajoute l'article/ $articleId au contenu, en quantité $qte
            $this->contenu[$article->getId()] = ['article' => $article, 'quantite' => $qte];
        } else {
            // ajoute l'article $articleId au contenu
            $this->contenu[$article->getId()] = ['article' => $article, 'quantite' => $this->contenu[$article->getId()]['quantite'] + $qte];
        }
    }

    public function supprimeArticle($articleId) {
        // supprimer l'article $articleId du contenu
        unset($this->contenu[$articleId]);
    }

    public function viderPanier() {
        $this->contenu = array();
    }

    public function validerPanier() {
        $this->contenu = array();
    }

    public function getTotal() {
        $this->contenu = array();
    }

}
