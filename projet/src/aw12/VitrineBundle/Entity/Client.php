<?php

namespace aw12\VitrineBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Client
 */
class Client implements UserInterface, \Serializable {

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $nom;

    /**
     * @var string
     */
    private $mail;

    /**
     * @var string
     */
    private $password;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $commandes;
    private $administrateur;

    /**
     * Constructor
     */
    public function __construct() {
        $this->commandes = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return Client
     */
    public function setNom($nom) {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom() {
        return $this->nom;
    }

    /**
     * Set mail
     *
     * @param string $mail
     * @return Client
     */
    public function setMail($mail) {
        $this->mail = $mail;

        return $this;
    }

    /**
     * Get mail
     *
     * @return string
     */
    public function getMail() {
        return $this->mail;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return Client
     */
    public function setPassword($password) {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword() {
        return $this->password;
    }

    /**
     * Add commandes
     *
     * @param \aw12\VitrineBundle\Entity\Commande $commandes
     * @return Client
     */
    public function addCommande(\aw12\VitrineBundle\Entity\Commande $commandes) {
        $this->commandes[] = $commandes;

        return $this;
    }

    /**
     * Remove commandes
     *
     * @param \aw12\VitrineBundle\Entity\Commande $commandes
     */
    public function removeCommande(\aw12\VitrineBundle\Entity\Commande $commandes) {
        $this->commandes->removeElement($commandes);
    }

    /**
     * Get commandes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCommandes() {
        return $this->commandes;
    }

    public function eraseCredentials() {

    }

    public function getRoles() {
        if ($this->isAdministrateur()) {
            return array('ROLE_ADMIN');
        } else {
            return array('ROLE_USER');
        }
    }

    public function getSalt() {
        return null;
    }

    public function getUsername() {
        return $this->mail;
    }

    public function serialize() {
        return serialize(array($this->id, $this->mail, $this->password));
    }

    public function unserialize($serialized) {
        list ($this->id, $this->mail, $this->password) = unserialize($serialized);
    }

    public function isAdministrateur() {
        if ($this->administrateur) {
            return true;
        } else {
            return false;
        }
    }

    public function setAdministrateur($administrateur) {

        $this->administrateur = $administrateur;

        return $this;
    }

    public function getAdministrateur() {
        return $this->administrateur;
    }

    public function __toString() {// renvoyer une chaîne qui identifie de manière unique l’entité
        return $this->getMail(); // si l’attribut Intitule est unique pour chaque catégorie… }
    }

}
