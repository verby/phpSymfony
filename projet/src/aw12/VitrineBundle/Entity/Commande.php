<?php

namespace aw12\VitrineBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Commande
 */
class Commande {

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $date_com;

    /**
     * @var boolean
     */
    private $etat;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $lignes_article_commande;

    /**
     * @var \aw12\VitrineBundle\Entity\Client
     */
    private $client;

    /**
     * Constructor
     */
    public function __construct() {
        $this->lignes_article_commande = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set date_com
     *
     * @param \DateTime $dateCom
     * @return Commande
     */
    public function setDateCom($dateCom) {
        $this->date_com = $dateCom;

        return $this;
    }

    /**
     * Get date_com
     *
     * @return \DateTime
     */
    public function getDateCom() {
        return $this->date_com;
    }

    /**
     * Set etat
     *
     * @param boolean $etat
     * @return Commande
     */
    public function setEtat($etat) {
        $this->etat = $etat;

        return $this;
    }

    /**
     * Get etat
     *
     * @return boolean
     */
    public function getEtat() {
        return $this->etat;
    }

    /**
     * Add lignes_article_commande
     *
     * @param \aw12\VitrineBundle\Entity\LigneCommande $lignesArticleCommande
     * @return Commande
     */
    public function addLignesArticleCommande(\aw12\VitrineBundle\Entity\LigneCommande $lignesArticleCommande) {
        $this->lignes_article_commande[] = $lignesArticleCommande;

        return $this;
    }

    /**
     * Remove lignes_article_commande
     *
     * @param \aw12\VitrineBundle\Entity\LigneCommande $lignesArticleCommande
     */
    public function removeLignesArticleCommande(\aw12\VitrineBundle\Entity\LigneCommande $lignesArticleCommande) {
        $this->lignes_article_commande->removeElement($lignesArticleCommande);
    }

    /**
     * Get lignes_article_commande
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLignesArticleCommande() {
        return $this->lignes_article_commande;
    }

    /**
     * Set client
     *
     * @param \aw12\VitrineBundle\Entity\Client $client
     * @return Commande
     */
    public function setClient(\aw12\VitrineBundle\Entity\Client $client = null) {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \aw12\VitrineBundle\Entity\Client
     */
    public function getClient() {
        return $this->client;
    }

    public function __toString() {
        return $this->getId();
    }

}
