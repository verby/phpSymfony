<?php

namespace aw12\VitrineBundle\Services;

class MontantPanier {

    public function calculerMontant($panier) {
        $montant = 0;

        foreach ($panier->getContenu() as $idArticle => $articles) {

            $montant+=$articles['article']->getPrix() * $articles['quantite'];
        }
        return $montant;
    }

}
