<?php

namespace aw12\VitrineBundle\Services;

class Mail {

    private $mailer;
    private $templating;

    public function __construct(\Swift_Mailer $mailer, $templating) {
        $this->mailer = $mailer;
        $this->templating = $templating;
    }

    public function sendMail($fromEmail, $toEmail, $subject, $arts) {
        $message = \Swift_Message::newInstance()
                ->setSubject($subject)
                ->setFrom($fromEmail)
                ->setTo($toEmail)
                ->setBody($this->templating->render('VitrineBundle:Emails:registration.html.twig', [
                    'articles' => $arts,
                    'subject' => $subject
                        ]
                ), 'text/html'
        );
        $this->mailer->send($message);
    }

}
